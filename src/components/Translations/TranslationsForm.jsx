import { useForm } from "react-hook-form" 
// the form for the translations
const TranslationsForm = ({onTranslation}) => {
    const {register, handleSubmit} = useForm()

    const onSubmit = ({translationNotes}) => onTranslation(translationNotes)
    
    return(
        <form onSubmit={ handleSubmit(onSubmit) }>
            <fieldset>
                <label htmlFor="translation-notes">Translation note: </label>
                <input type="text" {...register('translationNotes')} maxLength={40} placeholder="Enter text for translation"  />
                <button type="submit">Translate</button>
            </fieldset>
    
        </form>
    )
}
export default TranslationsForm