// This function sorts the correct images whit the correct letters and removes all unessery symboles and numbers
const TranslationsSignImage = ({character}) => {
    if (/\d/.test(character) || /[^a-zA-Z]/.test(character)){
        return null
    }

    return (
        <img src={`img/${character}.png`} alt={character} />
    ) 
}
export default TranslationsSignImage