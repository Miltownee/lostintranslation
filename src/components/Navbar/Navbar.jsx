import { NavLink } from "react-router-dom"
import { useUser } from "../../context/UserContext"

// this function displays the navbar
const Navbar = () =>{

    const{user} = useUser()

    return(
        <nav>

            {user !== null &&

                <ul>
                    <li>
                        <NavLink to="/translations" className="btn">Translations</NavLink>
                    </li>
                    
                    <li>
                        <NavLink to="/profile" className="btn">Profile</NavLink>
                    </li>
                </ul>
            }

        </nav>
    )
}
export default Navbar