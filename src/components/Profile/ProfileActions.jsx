import { translationClearHistory } from "../../api/translation"
import { STORAGE_KEY_USER } from "../../const/storageKeys"
import { useUser } from "../../context/UserContext"
import { storageDelete, storageSave } from "../../utils/storage"

// this functions is for the profile actions
const ProfileActions = () =>{

    const { user,setUser} = useUser()

    // function for the log out button
const handleLogOutClick = () => {
    if(window.confirm('Are you sure?')) {
        storageDelete(STORAGE_KEY_USER)
        setUser(null)
    }
}
// function for clearing the history on the profile page
const handleClearHistoryClick = async () => {
    if(!window.confirm('Are you sure?\nThis can not be undone!')){
        return
    }
    const [clearError] = await translationClearHistory(user.id)
    if(clearError !== null){
        return
    }
    const updatedUser = {
        ...user,
        translations: []
    }
    storageSave(STORAGE_KEY_USER, updatedUser)
    setUser(updatedUser)
}
    return (

        <ul>
                <li><button onClick={handleClearHistoryClick}>Clear history</button></li>
                <li><button onClick={handleLogOutClick}>Logout</button></li>
        </ul>
    )

}

export default ProfileActions

