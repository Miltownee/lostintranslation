import ProfileTranslationHistoryItem from "./ProfileTranslationHistoryItem"
// Renders the profile history
// only renders the last 10 translations
const ProfileTranslationHistory = ({translations}) =>{

    const translationList = translations.map((translation, index) => <ProfileTranslationHistoryItem key={index + '-' + translation} translation={translation}/>)
    if(translationList.length > 10){
            translationList.splice(0,translationList.length - 10)
    }
    return (
        
        <section>
            <h4>Your translation history</h4>

            {translationList.length === 0 && <p>You have no translations yet.</p>}

            <ul>
                {translationList}
            </ul>
        </section>
    )
    
}

export default ProfileTranslationHistory