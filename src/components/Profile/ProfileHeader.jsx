// greeting function for user have logged in
const ProfileHeaders = ({username}) =>{
    return (
        
        <header>
            <h4>Hello, welcome back {username}</h4>
        </header>
    )
    
}

export default ProfileHeaders

