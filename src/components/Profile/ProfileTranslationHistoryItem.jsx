// Renders the ProfileTranslationHistory items
const ProfileTranslationHistoryItem = ({translation}) =>{
    return <li>{translation}</li>
}
export default ProfileTranslationHistoryItem