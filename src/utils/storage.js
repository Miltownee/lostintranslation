// function checks if key exist and if it is a string
const validateKey = key => {

    if(!key || typeof key !== 'string'){
        throw new Error("storageDelete: Invalide key was provided")
    }
} 
// function checks if the value is correct
export const storageSave = (key, value) => {
    validateKey(key)

    if(!value){
        throw new Error('storageSave: No value provided for ' + key)

    }
    sessionStorage.setItem(key, JSON.stringify(value))
}
// function that reads the key value.
export const storageRead = key => {
    validateKey(key)

    const data= sessionStorage.getItem(key)
    if(data){
        return JSON.parse(data)
    }
    return null
}
// function that deletes the key value.
export const storageDelete = key => {
    validateKey(key)

    sessionStorage.removeItem(key)
}