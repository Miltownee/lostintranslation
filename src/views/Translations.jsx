import { useState } from "react"
import { translationAdd } from "../api/translation"
import TranslationsForm from "../components/Translations/TranslationsForm"
import { STORAGE_KEY_USER } from "../const/storageKeys"
import { useUser } from "../context/UserContext"
import withAuth from "../hoc/withAuth"
import { storageSave } from "../utils/storage"
import TranslationsSignImage from '../components/Translations/TranslationsSignImage.jsx'

const Translations = () => {
    const [signs, setSigns] = useState(null)
    const {user, setUser} = useUser()

    const handleTranslationClicked = async (notes) => {

        const [error, updatedUser] = await translationAdd(user, notes)

        if(error !== null){
            return
        }
        // Keep UI state and server state in sync
        storageSave(STORAGE_KEY_USER, updatedUser)
        setUser(updatedUser)

        let images = []

        for (let i = 0; i < notes.length; i++) {
            images.push(<TranslationsSignImage 
                character={notes[i]}
                key={i + 1}
            />)
        }

        setSigns(images)
    };  
                return (
        <>
            <h1>Translations</h1>
            <section id="sign-options">

            </section>
            <section id="translation-notes">
                <TranslationsForm onTranslation={handleTranslationClicked}/>
            </section>
            <div className="translationImages">
                { signs }
            </div>
        </>
    )
}
export default withAuth(Translations)