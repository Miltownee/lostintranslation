import { createHeaders } from './index';
const apiURL = process.env.REACT_APP_API_URL



// this function check if user exist 
const checkForUser = async (username) => {
try {
    const response = await fetch(`${apiURL}?username=${username}`)
    if(!response.ok){
        throw new Error("Could not complete request.")
    }
        const data =  await response.json()
        return [ null, data ]
} catch (error) {

    return [error.message, []]
}
} 
// this function creates a new record with a user
const createUser = async (username) => {
    try {
    
        const response = await fetch(apiURL, { 
            method: 'POST',
            headers: createHeaders(),
            body: JSON.stringify({
                username,
                translations:[] 
            })
        })
        if(!response.ok){
            throw new Error("Could not create user whit username " + username)
        }
        const data =  await response.json()
        return [ null, data ]
    } catch (error) {
    
        return [error.message, []]
    }
    
} 
// this function checks if user exist, user gets logged in. Else it calls the createuser function

export const loginUser = async(username) =>{
    const [checkError, user] = await checkForUser(username)

        if( checkError !== null){
            return [checkError, null]
        }

        if(user.length > 0){

            return [null, user.pop()]
        }

        return await createUser(username)

    }

    // this function checks if the user exist though user id
export const userById = async (userId) => {
    try {
        const response = await fetch(`${apiURL}/${userId}`)
        if(!response.ok){
            throw new Error('Could not fetch user')
        }
        const user = await response.json()
        return [null, user]
    } catch (error) {
        return [error.message, null]
    }
}