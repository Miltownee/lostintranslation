const apiKey = process.env.REACT_APP_API_KEY;
// declares how content will be fetched
export const createHeaders = () =>{

    return {
        'Content-Type': 'application/json',
        'x-api-key': apiKey
    }
}
